package net.edu.loggers.models;

public enum EventType {
    INFO, ERROR;
}
