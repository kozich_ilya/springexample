package net.edu.loggers.models;

import org.springframework.beans.factory.annotation.Value;

import java.text.DateFormat;
import java.util.*;

public class Event {
    private String id = UUID.randomUUID().toString();
    private String message;
    private Date date;
    private DateFormat dateFormat;


    public Event(Date date, DateFormat dateFormat) {
        this.date = date;
        this.dateFormat = dateFormat;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Event { id = " + id + "; message = " + message + "; date = " + dateFormat.format(date) + " }";
    }
}
