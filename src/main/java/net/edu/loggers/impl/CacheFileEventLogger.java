package net.edu.loggers.impl;

import net.edu.loggers.models.Event;

import java.util.ArrayList;
import java.util.List;

public class CacheFileEventLogger extends FileEventLogger {
    private int cacheSize;
    private List<Event> cache;

    public CacheFileEventLogger(String fileName, int cacheSize) {
        super(fileName);

        this.cacheSize = cacheSize;
        cache = new ArrayList<>();
    }

    public void preDestroy() {
        if(cache.isEmpty())
            return;

        for(Event event : cache) {
            super.logEvent(event);
        }
    }

    @Override
    public void logEvent(Event event) {
        if(cache.size() < cacheSize) {
            cache.add(event);
        }
        else {
            super.logEvent(event);
            cache.clear();
        }
    }
}
