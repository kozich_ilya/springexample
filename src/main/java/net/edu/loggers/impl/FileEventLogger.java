package net.edu.loggers.impl;

import net.edu.loggers.EventLogger;
import net.edu.loggers.models.Event;
import org.apache.commons.io.FileUtils;

import java.io.*;

public class FileEventLogger implements EventLogger {
    private String fileName;
    private File file;

    public FileEventLogger(String fileName) {
        this.fileName = fileName;
    }

    public void init() throws IOException {
        this.file = new File(fileName);

        if(!file.canWrite()) {
            throw new IOException();
        }
    }

    @Override
    public void logEvent(Event event) {
        try {
            FileUtils.touch(file);
            FileUtils.writeStringToFile(file, event.getMessage() + "\n", true);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
