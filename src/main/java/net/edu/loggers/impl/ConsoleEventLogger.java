package net.edu.loggers.impl;

import net.edu.loggers.EventLogger;
import net.edu.loggers.models.Event;

public class ConsoleEventLogger implements EventLogger {
    @Override
    public void logEvent(Event event) {
        System.out.println(event);
    }
}
