package net.edu.loggers;

import net.edu.loggers.models.Event;

public interface EventLogger {
    void logEvent(Event event);
}
