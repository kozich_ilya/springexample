package net.edu;

import net.edu.loggers.*;
import net.edu.loggers.models.*;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.*;

import java.util.Map;

public class App {
    EventLogger eventLogger;
    Client client;
    Map<EventType, EventLogger> eventLoggerMap;

    public App(EventLogger eventLogger, Client client, Map<EventType, EventLogger> eventLoggerMap) {
        this.eventLogger = eventLogger;
        this.client = client;
        this.eventLoggerMap = eventLoggerMap;
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("bean-configuration.xml");

        App app = (App)context.getBean("app");

        Event event1 = context.getBean("event", Event.class);
        Event event2 = context.getBean("event", Event.class);
        event1.setMessage("Some event for client 1");
        event2.setMessage("Some event for client 2");

        app.logEvent(event1, EventType.ERROR);
        app.logEvent(event2, null);

        System.out.println(app.client.getName());

        context.close();
    }

    public void logEvent(Event event, EventType eventType) {
        EventLogger logger = eventLoggerMap.get(eventType);

        if(logger == null) {
            return;
        }

        logger.logEvent(event);
    }
}
